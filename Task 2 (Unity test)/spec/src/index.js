"use strict";

class Department {

    constructor() {
        this.managers = [];
        this.team = [];
    }

    showDepartment() {
        console.log(this);
    }

    salaryPayment() {
        for (var obj in this.team) {
            console.log(this.team[obj].firstName + ' ' + this.team[obj].lastName + ": got salary: " + this.team[obj].calcSalary());
        }
    }

}



class Employee {
    constructor(fName, lName, experience) {

        this.firstName = fName;
        this.lastName = lName;
        this.experience = experience;
        this.salary = 1000;

    }

    calcSalary() {
        if(this.experience >= 2 && this.experience < 5) {
            return this.salary + 200;
        } else if (this.experience > 5) {
            return this.salary * 1.2 + 500;
        }
    }

    addToManager(manager) {
        this.manager = manager;
        manager.team.push(this);
    }

    addToDepartmentTeam(department) {
        department.team.push(this);
    }

    showEmployee() {
        console.log(this);
    }

    toString() {
        return `${this.firstName} ${this.lastName}, manager: ${this.manager.lastName} experience: ${this.experience}`
    }

}

class Designer extends Employee {

    constructor(fName, lName, experience, effectiveness) {
        super(fName, lName, experience);
        this.effCoef = effectiveness;
        this._effCoef = 1;
    }

    get effCoef() {
        return this._effCoef;
    }

    set effCoef(value) {
        if(typeof value === "number" && value > 0 && value <= 1) {
            this._effCoef = value;
        } else {
            console.log('Воу воу, полегче');
        }
    }

    calcSalary() {
        return this.salary * this._effCoef;
    }

}

class Developer extends Employee {

    constructor(fName, lName, experience) {
        super(fName, lName, experience);
    }

}

class Manager extends Employee {

    constructor(fName, lName, experience) {
        super(fName, lName, experience);
        this.salaryBonus = 0;
        this.team = [];
    }

    calcSalary() {

        let developers = this.team.filter(employee => employee instanceof Developer).length;
        let designers = this.team.filter(employee => employee instanceof Designer).length;



        if(this.team.length > 5 && this.team.length <= 10 ) {
            this.salaryBonus += 200;
        } else if ( this.team.length > 10) {
            this.salaryBonus += 500;
        }

        if(developers > designers) {
            return (this.salary + this.salaryBonus) * 1.1;
        } else {
            return this.salary + this.salaryBonus;
        }

    }

    addToDepartment(department) {
        department.managers.push(this);
    }

}

module.exports.Developer = Developer;
module.exports.Manager = Manager;
module.exports.Designer = Designer;

// let Office = new Department();
//
// let Designer1 = new Designer('Vasya', 'Pupkin', 3, .9);
// let Designer2 = new Designer('Petya', 'Ivanov', 1, .7);
// let Designer3 = new Designer('Vanya', 'Ivanov', 1, .5);
// let Developer4 = new Developer('Vasya', 'Ivanov', 1);
// let Developer5 = new Developer('Vlad', 'Mususashvili', 20);
// let Developer6 = new Developer('Anton', 'Mususashvili', 20);
// let Developer7 = new Developer('Ignat', 'Mususashvili', 20);
// let Manager8 = new Manager('Artyom', 'Petrov', 5);
// let Manager9 = new Manager('Tony', 'Stark', 3);
//
// Designer1.calcSalary();
// Designer1.showEmployee();
// Designer1.addToManager(Manager8);
// Designer1.addToDepartmentTeam(Office);
//
// Designer2.calcSalary();
// Designer2.showEmployee();
// Designer2.addToManager(Manager8);
// Designer2.addToDepartmentTeam(Office);
//
// Designer3.calcSalary();
// Designer3.showEmployee();
// Designer3.addToManager(Manager8);
// Designer3.addToDepartmentTeam(Office);
//
// Developer4.calcSalary();
// Developer4.showEmployee();
// Developer4.addToManager(Manager8);
// Developer4.addToDepartmentTeam(Office);
//
// Developer5.calcSalary();
// Developer5.showEmployee();
// Developer5.addToManager(Manager8);
// Developer5.addToDepartmentTeam(Office);
//
// Developer6.calcSalary();
// Developer6.showEmployee();
// Developer6.addToManager(Manager8);
// Developer6.addToDepartmentTeam(Office);
//
// Developer7.calcSalary();
// Developer7.showEmployee();
// Developer7.addToManager(Manager8);
// Developer7.addToDepartmentTeam(Office);
//
// Manager8.calcSalary();
// Manager8.showEmployee();
// Manager8.addToDepartment(Office);
// Manager8.addToDepartmentTeam(Office);
//
// Manager9.calcSalary();
// Manager9.showEmployee();
// Manager9.addToDepartment(Office);
// Manager9.addToDepartmentTeam(Office);
//
// Office.showDepartment();
// Office.salaryPayment();

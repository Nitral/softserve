const {Developer, Manager, Designer} = require('./src/index');

describe("Salary calculation", function() {

    beforeEach(function () {
        this.Petya = new Developer('Petya', 'Ivanov', 3);
    });

    it('Developer\'s salary with 3 years exp should be 1200$ ', function() {

        expect(this.Petya.calcSalary()).toEqual(1200);

    });

});

describe("Manager\'s team", function() {

    beforeEach(function () {
        this.Anton = new Manager('Anton', 'Petrov', 3);
        this.Petya = new Developer('Petya', 'Ivanov', 3);
        this.Petya.addToManager(this.Anton);
        this.Vasya = new Developer('Vasya', 'Pupkin', 6);
        this.Vasya.addToManager(this.Anton);
        this.Sasha = new Developer('Sasha', 'Pistoletov', 1);
        this.Sasha.addToManager(this.Anton);
    });

    it('Manager\'s team shoul contain 3 persons ', function() {

        expect(this.Anton.team.length).toEqual(3);

    });

});


describe("Manager\'s salary with 3 developers and 2 designers should be 1540", function() {

    beforeEach(function () {
        this.Anton = new Manager('Anton', 'Petrov', 3);

        this.Petya = new Developer('Petya', 'Ivanov', 3);
        this.Petya.addToManager(this.Anton);
        this.Vasya = new Developer('Vasya', 'Pupkin', 6);
        this.Vasya.addToManager(this.Anton);
        this.Sasha = new Developer('Sasha', 'Pistoletov', 1);
        this.Sasha.addToManager(this.Anton);
        this.Katya = new Developer('Katya', 'Pistoletov', 1);
        this.Katya.addToManager(this.Anton);
        this.Vlad = new Designer('Vlad', 'Pupkin', 6);
        this.Vlad.addToManager(this.Anton);
        this.Ujin = new Designer('Ujin', 'Pistoletov', 1);
        this.Ujin.addToManager(this.Anton);
    });

    it('Manager\'s team shoul contain 3 persons ', function() {

        expect(this.Anton.calcSalary()).toEqual(1320);

    });

});

const { Developer, Designer, Manager} = require('./task-1');

var http = require('http'),
    bodyParser = require("body-parser");
express = require('express');

var app = express();
var jsonParser = bodyParser.json();

let employees_counter = 0;
let managers_counter = 0;

let employees = [];
let managers = [];

app.get('/api/v1/employees', jsonParser, (req, res, next) => {
    let employeesObj = employees.map(employee => employee);
    res.json(employeesObj);
});

app.get('/api/v1/employees/:id', jsonParser, (req, res, next) => {
    let employee = employees.find(function (employee) {
        return employee.id === Number(req.params.id);
    });
    employee.calcSalary();
    let resp = {...employee};
    resp.salary = employee.calcSalary();
    res.send(resp);
});

app.post('/api/v1/employees', jsonParser, (req, res, next) => {
    let employee;
    if( req.body.type === 'developer' ) {
        employee = new Developer(req.body.firstName, req.body.lastName, req.body.salary, req.body.experience);
        employee.type = req.body.type;
        employee.id = employees_counter;
    } else if(req.body.type === 'designer') {
        employee = new Designer(req.body.firstName, req.body.lastName, req.body.salary, req.body.experience, req.body.effCoef);
        employee.type = req.body.type;
        employee.id = employees_counter;
    } else if(req.body.type === 'manager') {
        employee = new Manager(req.body.firstName, req.body.lastName, req.body.salary, req.body.experience);
        employee.type = req.body.type;
        employee.id = employees_counter;
    }
    employees_counter++;
    employees.push(employee);
    res.send(employee);
});

app.get('/api/v1/managers', jsonParser, (req, res, next) => {
    let managersObj = managers.map(manager => manager);
    res.json(managersObj);
});

app.post('/api/v1/managers', jsonParser, (req, res, next) => {
    let manager = new Manager(req.body.firstName, req.body.lastName, req.body.salary, req.body.experience);
    manager.type = req.body.type;
    manager.id = managers_counter;
    managers.push(manager);
    managers_counter++;
    res.send(manager);
});

app.get('/api/v1/managers/:id', jsonParser, (req, res, next) => {
    let manager = managers.find(function (manager) {
        return manager.id === Number(req.params.id);
    });
    manager.calcSalary();
    let resp = {...manager};
    resp.salary = manager.calcSalary();
    res.send(manager);
});

app.get('/api/v1/managers/:id/team', jsonParser, (req, res, next) => {
    let manager = managers.find(function (manager) {
        return manager.id === Number(req.params.id);
    });
    res.send(manager.team);
});

app.post('/api/v1/managers/:id/team', jsonParser, (req, res, next) => {
    let manager = managers.find(function (manager) {
        return manager.id === Number(req.params.id);
    });
    let employee = employees.find(function (employee) {
        return employee.id === req.body.employee_id;
    });
    manager.team.push(employee);
    res.send(manager.team);
});

http.createServer(app).listen(3000);
